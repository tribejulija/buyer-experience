---
  components:
    - name: copy
      data:
        block:
          - hide_horizontal_rule: true
            no_margin_bottom: true
            text: |
              ## GitLab Support Service Levels {#gitlab-support-service-levels}

              ### Trials Support {#trials-support}

              Trial licenses do not include support at any level. If part of your evaluation of GitLab includes evaluating support expertise or SLA performance, please consider [contacting Sales](/sales/){data-ga-name="sales" data-ga-location="body"} to discuss options.

              ### Standard Support (Legacy) {#standard-support-legacy}

              Standard Support is included in Legacy GitLab self-managed **Starter**, and GitLab.com **Bronze** plans. It includes 'Next business day support' which means you can expect a reply to your ticket within 24 hours 24x5. (See [Support Staffing Hours](#definitions-of-gitlab-global-support-hours)).

              Please submit your support request through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="standard support"}. When you receive your license file, you will also receive an email address to use if you need to reach Support and the web form can't be accessed for any reason.

              ### Priority Support {#priority-support}

              Priority Support is included with all self-managed and SaaS GitLab [Premium and Ultimate](/pricing/){data-ga-name="pricing" data-ga-location="body"} purchases. These plans receive **Tiered Support response times**:

              | [Support Impact](#definitions-of-support-impact) | First Response Time SLA  | Hours | How to Submit |
              |-----------------|-------|------|------------------------------------------------|
              | Emergency (Your GitLab instance is completely unusable) | 30 minutes | 24x7 | [Please trigger an emergency](#how-to-trigger-emergency-support){data-ga-name="emergency support" data-ga-location="priority support"} |
              | Highly Degraded (Important features unavailable or extremely slow; No acceptable workaround) | 4 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="high priority support"}. |
              | Medium Impact   | 8 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="medium priority support"}. |
              | Low Impact      | 24 hrs| 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/){data-ga-name="support portal" data-ga-location="low priority support"}. |

              Self-managed Premium and Ultimate may also receive:

              - **Support for Scaled Architecture**: A Support Engineer will work with your technical team around any issues encountered after an implementation of a scaled architecture is completed in cooperation with our Customer Success team.
              - **Live upgrade assistance**: Request to schedule an upgrade time with GitLab to move from GitLab version to updated version. We'll host a live screen share session to help you through the process and ensure there aren't any surprises. Learn [how to schedule an upgrade.](scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance){data-ga-name="schedule upgrade" data-ga-location="body"}

              #### Definitions of Support Impact {#definitions-of-support-impact}

              - **Severity 4** - Questions or Clarifications around features or documentation or deployments (24 hours) *Minimal or no Business Impact.* Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.
              - **Severity 3** - Something is preventing normal GitLab operation (8 hours) *Some Business Impact.* Important GitLab features are unavailable or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.
              - **Severity 2** - GitLab is Highly Degraded (4 hours) *Significant Business Impact.* Important GitLab features are unavailable or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.
              - **Severity 1** - Your instance of GitLab is unavailable or completely unusable (30 Minutes) *A GitLab server or cluster in production is not available, or is otherwise unusable.* An emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.

              ##### Ticket severity and customer priority {#ticket-severity-and-customer-priority}

              When submitting a ticket to support, you will be asked to select both a severity and a priority. The severity will display definitions aligning with [the above section](#definitions-of-support-impact). Priority is lacking any definition - this is because the priority is whatever you and your organization define it as. The customer priority (shorted on the forms as priority) is Support's way of asking the impact or importance the ticket has to your organization and its needs (business, technical, etc.).

              **Note**: The [US Federal support portal](https://federal-support.gitlab.com/) is often setup differently than the [Global support portal](https://support.gitlab.com) and might differ in the questions asked via the ticket submission forms.

              ##### How to Trigger Emergency Support {#how-to-trigger-emergency-support}

              To trigger emergency support you **must send a new email** to the emergency contact address provided with your GitLab license. When your license file was sent to your licensee email address, GitLab also sent a set of addresses to reach Support for emergency requests. You can also ask your Technical Account Manager or sales rep for these addresses.

              - **Note:** CCing the address on an existing ticket or forwarding it will **not** page the on-call engineer.

              It is preferable to [include any relevant screenshots/logs in the initial email](#working-effectively-in-support-tickets){data-ga-name="Effective support tickets" data-ga-location="body"}. However if you already have an open ticket that has since evolved into an emergency, please include the relevant ticket number in the initial email.

               - **Note:** For GitLab.com customers our infrastructure team is on-call 24/7 - please check [status.gitlab.com](https://status.gitlab.com){data-ga-name="GitLab status" data-ga-location="body"} before contacting Support.

              Once an emergency has been resolved, GitLab Support will close the emergency ticket. If a follow up is required post emergency, GitLab Support will either continue the conversation via a new regular ticket created on the customer's behalf, or via an existing related ticket.

              #### Definition of Scaled Architecture {#definition-of-scaled-architecture}
              Scaled architecture is defined as any GitLab installation that separates services for the purposes of resilience, redundancy or scale. As a guide, our [2,000 User Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html){data-ga-name="Reference architecture 2k" data-ga-location="body"} (and higher) would fall under this category.

              [Priority Support](#priority-support){data-ga-name="priority support" data-ga-location="scaled architecture"} is required to receive assistance in troubleshooting a scaled implementation of GitLab.

              ##### Scaled Architecture - Omnibus and Source Installations {#scaled-architecture---omnibus-and-source-installations}
              In Omnibus and Source installations, Scaled Architecture is any deployment with multiple GitLab application nodes. This does not include external services such as Amazon RDS or ElastiCache.

              |  **Service** | **Scaled Architecture** | **Not Scaled Architecture** |
              | --- | --- | --- |
              |  Application (GitLab Rails) | Using multiple application nodes to provide resilience, scalability or availability | Using a single application node |
              |  Database | Using multiple database servers with Consul and PgBouncer | Using a single separate database or managed database service |
              |  Caching | Using Redis HA across multiple servers with Sentinel | Using a single separate server for Redis or a managed Redis service |
              |  Repository / Object Storage | Using one or more separate Gitaly nodes. Using NFS across multiple application servers. | Storing objects in S3 |
              |  Job Processing | Using one or more separate Sidekiq nodes | Using Sidekiq on the same host as a single application node |
              |  Load Balancing | Using a Load Balancer to balance connections between multiple application nodes | Using a single application node |
              |  Monitoring | Not considered in the definition of Scaled Architecture | Not considered in the definition of Scaled Architecture |

              #### Service Level Agreement (SLA) details {#service-level-agreement-sla-details}

              - GitLab offers 24x5 support (24x7 for Priority Support Emergency tickets) bound by the SLA times listed above.
              - The SLA times listed are the time frames in which you can expect the first response.
              - GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the SLA times are *not* to be considered as an expected time-to-resolution.

              #### Definitions of GitLab Global Support Hours {#definitions-of-gitlab-global-support-hours}

              - **24x5** - GitLab Support Engineers are actively responding to tickets Sunday 3pm Pacific Time through Friday 5pm Pacific Time.
              - **24x7** - For [Emergency Support](#definitions-of-support-impact){data-ga-name="emergency support" data-ga-location="support hours"} there is an engineer on-call 24 hours a day, 7 days a week.

              These hours are the SLA times when selecting 'All Regions' for 'Preferred Region for Support' within the [GitLab Global Support Portal](https://support.gitlab.com){data-ga-name="support portal" data-ga-location="support hours"}.

              #### Effect on Support Hours if a preferred region for support is chosen {#effect-on-support-hours-if-a-preferred-region-for-support-is-chosen}

              When submitting a new ticket, you will select a 'preferred region for support'. This helps us assign Support Engineers from your region and means you'll be more likely to receive replies in your business hours (rather than at night).
              If you choose a preferred region, the Support Hours for the purposes of your ticket SLA are as follows:

              - Asia Pacific (APAC): 09:00 to 21:00 AEST (Brisbane), Monday to Friday
              - Europe, Middle East, Africa (EMEA): 08:00 to 18:00 CET Amsterdam, Monday to Friday
              - Americas (AMER): 05:00 to 17:00 PT (US & Canada), Monday to Friday

              Customers who select 'All regions' as their preferred region will receive SLAs of 24x5 as described above.

              ### Customer Satisfaction {#customer-satisfaction}
              24 hours after a ticket is Solved or automatically closed due to a lack of activity, a Customer Satisfaction survey will be sent out.
              We track responses to these surveys through Zendesk with a target of 95% customer satisfaction.

              Support Management regularly reviews responses, and may contact customers who leave negative reviews for more context.

              ### Phone and video call support {#phone-and-video-call-support}
              GitLab does not offer support via inbound or on-demand calls.

              GitLab Support Engineers communicate with you about your tickets primarily through updates in the tickets themselves. At times it may be useful and important to conduct a call, video call, or screensharing session with you to improve the progress of a ticket. The support engineer may suggest a call for that reason. You may also request a call if you feel one is needed. Either way, the decision to conduct a call always rests with the support engineer, who will determine:

              * whether a call is necessary; and
              * whether we have sufficient information for a successful call.

              Once the decision has been made to schedule a call, the support engineer will:

              1. Send you a link (through the ticket) to our scheduling platform or, in the case of an emergency, a direct link to start the call.
              1. Update you through the ticket with: (a) an agenda and purpose for the call, (b) a list of any actions that must be taken to prepare for the call, and (c) the maximum time allowed for the call. _Please expect that the call will end as soon as the stated purpose has been achieved or the time limit has been reached, whichever occurs first._

              During a screensharing session Support Engineers will act as a trusted advisor: providing troubleshooting steps and inviting you to run commands to help gather data or help resolve technical issues. At no time will a GitLab Support Engineer ask to take control of your computer or to be granted direct access to your GitLab installation.

              **NOTE:** Calls scheduled by GitLab Support are on the Zoom platform. If you cannot use Zoom, you can request a Cisco Webex link. If neither of these work for you, GitLab Support can join a call on the following platforms: Microsoft Teams, Google Hangouts, Zoom, Cisco Webex. Other video platforms are not supported.

              ***Please Note:***  Attempts to reuse a previously-provided scheduling link to arrange an on-demand call will be considered an abuse of support, and will result in such calls being cancelled.
